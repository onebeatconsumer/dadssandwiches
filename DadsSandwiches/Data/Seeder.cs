﻿using System;
using System.Collections.Generic;
using System.Linq;
using DadsSandwiches.Models;
using Raven.Abstractions.Data;

namespace DadsSandwiches.Data
{
    public static class Seeder
    {
        public static void DropData()
        {
            var session = DadsMvcApplication.RavenStore.OpenSession();

            var tagName = DadsMvcApplication.RavenStore.Conventions.GetTypeTagName(typeof(Restaurant)); 
            var query = string.Format("Tag:[[{0}]]", tagName); 
            session.Advanced.DatabaseCommands.DeleteByIndex("Raven/DocumentsByEntityName", new IndexQuery { Query = query }, true);
            DadsMvcApplication.RavenStore.DatabaseCommands.Delete("Raven/Hilo/restaurants", null);

            tagName = DadsMvcApplication.RavenStore.Conventions.GetTypeTagName(typeof(CateringInquiry));
            query = string.Format("Tag:[[{0}]]", tagName); 
            session.Advanced.DatabaseCommands.DeleteByIndex("Raven/DocumentsByEntityName", new IndexQuery { Query = query }, true);
            DadsMvcApplication.RavenStore.DatabaseCommands.Delete("Raven/Hilo/cateringinquiries", null);

            session.SaveChanges();
        }

        public static void Seed()
        {
            var session = DadsMvcApplication.RavenStore.OpenSession();

            var onS = new Restaurant
            {
                Name = "S St.",
                Slug = "on-S",
                Address = new Address { Street = "1310 S St.", City = "Sacramento", State = "CA", Zip = "95811"},
                Description = @"Sacramento's Favorite Sandwich Dive!",
                Hours = @"Mon-Fri 7am-4pm<br/>Sat 10am-4pm",
                Image = "dads-on-s-640.jpg",
                MapUri = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1310+S+St,+Sacramento,+CA&aq=&sll=38.561855,-121.465934&sspn=0.064966,0.132093&vpsrc=0&ie=UTF8&hq=&hnear=1310+S+St,+Sacramento,+California+95811&t=h&z=17&iwloc=A",
                Phone = "(916) 448-3237"
            };
            var onJ = new Restaurant
            {
                Name = "J St.",
                Slug = "on-J",
                Address = new Address { Street = "1004 J St.", City = "Sacramento", State = "CA", Zip = "95814" },
                Description = @"Dad's New Location! Bigger Menu!",
                Hours = @"Mon-Fri 7am-4pm<br/>Sat 10am-4pm",
                Image = "dads-on-j-640.jpg",
                MapUri = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1003+J+Street,+Sacramento,+CA&aq=0&oq=1003+J+St.+&sll=38.58014,-121.493248&sspn=0.016237,0.033023&vpsrc=0&ie=UTF8&hq=&hnear=1003+J+St,+Sacramento,+California+95814&t=h&z=17&iwloc=A",
                Phone = "(916) 123-4567"
            };

            #region Initialize Restaurants, with empty Menus with empty Sections, with empty Itmes

            onJ.Menus = new List<Menu>
                           {
                               new Menu
                                   {
                                       Name = "Menu",
                                       Sections = new List<MenuSection>
                                                      {
                                                          new MenuSection
                                                              {
                                                                  Name = "Specials",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Hot Sandwiches",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Cold Sandwiches",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Breakfast Sandwiches",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Salads",
                                                                  Order = 1,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Yummies",
                                                                  Order = 2,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Beverages",
                                                                  Order = 3,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                      }
                                   }
                           };
            onS.Menus = new List<Menu>
                           {
                               new Menu
                                   {
                                       Name = "Menu",
                                       Sections = new List<MenuSection>
                                                      {
                                                          new MenuSection
                                                              {
                                                                  Name = "Specials",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Hot Sandwiches",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Cold Sandwiches",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Breakfast Sandwiches",
                                                                  Order = 0,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Salads",
                                                                  Order = 1,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Yummies",
                                                                  Order = 2,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                          new MenuSection
                                                              {
                                                                  Name = "Beverages",
                                                                  Order = 3,
                                                                  Items = new List<MenuItem>()
                                                              },
                                                      }
                                   }
                           };

            #endregion

            #region Create all Menu Items

            var hotBreakfastSandwiches = new List<MenuItem>
                                             {
                                                 new MenuItem
                                                     {
                                                         Name = "American Standard",
                                                         Description = "Eggs and American cheese on wheat.",
                                                         Price = new decimal(4.00),
                                                         IconUri = "/",
                                                         Updated = DateTime.Now
                                                     },
                                                 new MenuItem
                                                     {
                                                         Name = "Dad's Breakfast Classic",
                                                         Description = "Eggs, bacon, and cheddar cheese on sourdough.",
                                                         Price = new decimal(5.00),
                                                         IconUri = "/",
                                                         Updated = DateTime.Now
                                                     },
                                                 new MenuItem
                                                     {
                                                         Name = "Boss Hog",
                                                         Description =
                                                             "Eggs, ham, provolone cheese, spinach, and tomator on sourdough.",
                                                         Price = new decimal(5.25),
                                                         IconUri = "/",
                                                         Updated = DateTime.Now
                                                     },
                                                 new MenuItem
                                                     {
                                                         Name = "Meatless Marvel",
                                                         Description =
                                                             "Eggs, cheddar cheese, aVaocado, spinach, and tomato on organic wheat.",
                                                         Price = new decimal(5.00),
                                                         IconUri = "/",
                                                         Updated = DateTime.Now
                                                     },
                                                 new MenuItem
                                                     {
                                                         Name = "Big Daddy",
                                                         Description =
                                                             "Eggs, tri-tip, swiss cheese, roasted mushrooms and red onions on a roll.",
                                                         Price = new decimal(6.50),
                                                         IconUri = "/",
                                                         Updated = DateTime.Now
                                                     },
                                                 new MenuItem
                                                     {
                                                         Name = "Hacienda Hottie",
                                                         Description =
                                                             "Eggs, bacon, pepper jack cheese, roasted red bell peppers, red onion, and fresh jalapeno on sourdough.",
                                                         Price = new decimal(6.00),
                                                         IconUri = "/",
                                                         Updated = DateTime.Now
                                                     }
                                             };
            var hotSandwiches = new List<MenuItem>
                                    {
                                        new MenuItem
                                            {
                                                Name = "Hot Tuna",
                                                Description =
                                                    "Albacore tuna, cheddar and monterey jack cheese, red onions, black olives, tomato, brown mustard, and pepper plant sauce on sourdough. (spicy on request)",
                                                Price = new decimal(6.50),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Bad Breath Special",
                                                Description =
                                                    "Roast Beef, walnuts, blue cheese, red onions, roasted red bell peppers, mushrooms, garliC spread, brown mustard, pepper plant and horseradish sauce on organiC rye.",
                                                Price = new decimal(7.00),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Hot Blonde",
                                                Description =
                                                    "Chicken, avocado, swiss cheese, tomato, red onions, spinach, cucumber, garlic spread, brown mustard, and pepper plant sauce on sourdough.",
                                                Price = new decimal(7.25),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Dad's Rueben",
                                                Description =
                                                    "Pastrami, swiss cheese, sauerkraut, brown mustard, and Dad's own Thousand Island dressing on ogranic rye.",
                                                Price = new decimal(7.00),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Blue Collar Dream",
                                                Description =
                                                    "Tri-tip, monterey jack, black olives, red onion, roasted red bell peppers, mushrooms, garlic spread, brown mustard, and pepper plant sauce on a roll.",
                                                Price = new decimal(7.75),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Chunky Chicken",
                                                Description =
                                                    "Chicken, walnuts, provolone cheese, Dad's own pesto, tomato, and garlic spread on sourdough.",
                                                Price = new decimal(7.00),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Angry Road Man",
                                                Description =
                                                    "Turkey, bacon, swiss cheese, red onion, roasted red bell peppers, mushrooms, tomato, black olives, mayo, and brown mustard on sourdough.",
                                                Price = new decimal(7.50),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "All American",
                                                Description =
                                                    "Ham, american cheese, tomato, red onion, pickle, lettuce, mayo, and yellow mustard on a roll.",
                                                Price = new decimal(6.25),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Bikini Kill",
                                                Description =
                                                    "Turkey, ham, cheddar and cream cheese, red onion, tomato, roasted red bell peppers, and brown mustard on sourdough.",
                                                Price = new decimal(7.25),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Lockdown",
                                                Description =
                                                    "Chicken, bacon, blue cheese, red onion, roasted mushrooms, mayo, and brown mustard on organic wheat.",
                                                Price = new decimal(7.25),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Day Off",
                                                Description =
                                                    "Bacon, cream cheese, spinach, tomato, roasted mushrooms, brown mustard, and pepper plant sauce on focaccia.",
                                                Price = new decimal(7.25),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Fat Elvis",
                                                Description = "Peanut butter, banana, and local honey on white bread.",
                                                Price = new decimal(5.00),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Dad's Grilled Cheese",
                                                Description = "Your choice of cheese and bread, grilled to perfection!",
                                                Price = new decimal(4.00),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            },
                                        new MenuItem
                                            {
                                                Name = "Dad's Hot Classic",
                                                Description =
                                                    "Your choice of meat, cheese, and bread, fixed up with tomato, red onion, roasted red bell peppers, and mushrooms, garlic spread, brown mustard and pepper plant sauce.",
                                                Price = new decimal(7.00),
                                                IconUri = "/",
                                                Updated = DateTime.Now
                                            }
                                    };
            var coldSandwiches = new List<MenuItem>
                                     {
                                         new MenuItem
                                             {
                                                 Name = "Dad's BLT",
                                                 Description =
                                                     "Bacon, lettuce, tomato, with mayo on toasted organic wheat.",
                                                 Price = new decimal(6.25),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "California Carnivore",
                                                 Description =
                                                     "Roast beef and pastrami, cheddar, lettuce, tomato, red onion, sprouts, cucumber, horseradish, brown mustard, and mayo on a roll.",
                                                 Price = new decimal(6.75),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Dad's Egg Salad",
                                                 Description =
                                                     "Egg Salad, monterey jack, roasted red bell peppers, red onion, lettuce, sprouts, mayo, yellow mustard, and Hungarian paprika on organic wheat.",
                                                 Price = new decimal(5.50),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Green Machine",
                                                 Description =
                                                     "Avocado, cream cheese, roasted red bell peppers, cucumber, sprouts, tomato, red onion, lettuce, wax peppers, black olives with garlic spread, brown mustard, and pepper plant sauce on focaccia bread.",
                                                 Price = new decimal(7.00),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Sub-No-Way",
                                                 Description =
                                                     "Ham, roast beef, and turkey, cheddar & swiss cheese, lettuce, tomato, red onion, pickle, wax peppers, black olives, mayo, brown mustard, and red wine vinaigrette on a roll.",
                                                 Price = new decimal(7.25),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Opa!",
                                                 Description =
                                                     "Salami, feta cheese, roasted red bell peppers, tomato, block olives, red onion, lettuce, wax peppers, garlic spread, and balsamic vinaigrette on facaccia.",
                                                 Price = new decimal(6.25),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Honky Tonk Hero",
                                                 Description =
                                                     "Potato chips, american cheese, pickles, lettuce, mayo, and yellow mustard on white bread (+ Ham $1.00).",
                                                 Price = new decimal(5.00),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Day After",
                                                 Description =
                                                     "Turkey, cream cheese, dried cranberries, red onion, tomato, lettuce, mayo, and brown mustard on organic wheat.",
                                                 Price = new decimal(6.25),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Besto Pesto",
                                                 Description =
                                                     "Pesto, walnuts, cheddar cheese, tomato, cucumber, lettuce, tomato, and mayo on sourdough bread.",
                                                 Price = new decimal(5.75),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Dad's Chicken Salad",
                                                 Description =
                                                     "Chicken salad, walnuts, provolone, cucumber, lettuce, tomato, and mayo on organic wheat.",
                                                 Price = new decimal(6.50),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Getaway",
                                                 Description =
                                                     "Roast beef, cream cheese, tomato, red onion, lettuce, horseradish sauce, and brown mustard on facaccia bread.",
                                                 Price = new decimal(6.25),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Dad's PB & J",
                                                 Description =
                                                     "Organic peanut butter and grape jelly. For your inner 10yr old purist.",
                                                 Price = new decimal(4.00),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Dad's Cold Classic",
                                                 Description =
                                                     "Your choice of meat, cheese, and bread, with lettuce, tomato, pickles, red onion, mayo, and yellow or brown mustard.",
                                                 Price = new decimal(6.00),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             },
                                         new MenuItem
                                             {
                                                 Name = "Turnpike",
                                                 Description =
                                                     "Ham, salami, provolane cheese, lettuce, tomato, pickles, onion, mayo, and balsamic vinaigrette on a roll.",
                                                 Price = new decimal(7.00),
                                                 IconUri = "/",
                                                 Updated = DateTime.Now
                                             }
                                     };
            var salads = new List<MenuItem>
                             {
                                 new MenuItem
                                     {
                                         Name = "Dad's Caesar",
                                         Description =
                                             "Romaine lettuce, parmesan cheese, house-made croutons, with Dad's Caesar dressing.",
                                         Price = new decimal(5.50),
                                         IconUri = "/",
                                         Updated = DateTime.Now
                                     },
                                 new MenuItem
                                     {
                                         Name = "Greek",
                                         Description =
                                             "Romain lettuce, feta cheese, tomato, black olives, red onions, wax peppers, and roasted red bell peppers with Dad's house-made balsamic vinaigrette.",
                                         Price = new decimal(5.50),
                                         IconUri = "/",
                                         Updated = DateTime.Now
                                     },
                                 new MenuItem
                                     {
                                         Name = "Kitchen Sink",
                                         Description =
                                             "Romain lettuce, turkey, ham, hard boiled eggs, cheddar and swiss cheese, tomato, red onion, cucumber, black olives, wax peppers, and house-made croutons with a red wine vinaigrette.",
                                         Price = new decimal(5.50),
                                         IconUri = "/",
                                         Updated = DateTime.Now
                                     },
                                 new MenuItem
                                     {
                                         Name = "Dad's Spinach",
                                         Description =
                                             "Spinach, feta cheese, walnuts, dried cranberries, with a citrus vinaigrette.",
                                         Price = new decimal(5.50),
                                         IconUri = "/",
                                         Updated = DateTime.Now
                                     },
                                 new MenuItem
                                     {
                                         Name = "Sacto",
                                         Description =
                                             "Romain lettuce, tomato, red onion, cucumber, sprouts, wax peppers, black olives, roasted red bell peppers, with house-made ranch dressing.",
                                         Price = new decimal(5.00),
                                         IconUri = "/",
                                         Updated = DateTime.Now
                                     },
                                 new MenuItem
                                     {
                                         Name = "Chomper",
                                         Description =
                                             "Romain lettuce, spinach, walnuts, granny smith apples, blue cheese, red onion, with a red wine vinaigrette. ",
                                         Price = new decimal(5.75),
                                         IconUri = "/",
                                         Updated = DateTime.Now
                                     }
                             };
            var yummies = new List<MenuItem>
                              {
                                  new MenuItem
                                      {
                                          Name = "Chips",
                                          Description = "Any kind you want.",
                                          Price = new decimal(.95),
                                          IconUri = "/",
                                          Updated = DateTime.Now
                                      },
                                  new MenuItem
                                      {
                                          Name = "House-made Cookies",
                                          Description = "Assorted types.",
                                          Price = new decimal(1.50),
                                          IconUri = "/",
                                          Updated = DateTime.Now
                                      },
                                  new MenuItem
                                      {
                                          Name = "Pastries, Muffins, and Brownies",
                                          Description = "House-made magic!",
                                          Price = new decimal(2.00),
                                          IconUri = "/",
                                          Updated = DateTime.Now
                                      }
                              };
            var beverages = new List<MenuItem>
                                {
                                    new MenuItem
                                        {
                                            Name = "Soda",
                                            Description = "Any kind you want.",
                                            Price = new decimal(1.00),
                                            IconUri = "/",
                                            Updated = DateTime.Now
                                        },
                                    new MenuItem
                                        {
                                            Name = "Tea",
                                            Description = "Assorted types.",
                                            Price = new decimal(1.50),
                                            IconUri = "/",
                                            Updated = DateTime.Now
                                        },
                                    new MenuItem
                                        {
                                            Name = "Juice",
                                            Description = "Assorted types.",
                                            Price = new decimal(1.50),
                                            IconUri = "/",
                                            Updated = DateTime.Now
                                        },
                                    new MenuItem
                                        {
                                            Name = "Energy Drinks",
                                            Description = "Assorted types.",
                                            Price = new decimal(3.00),
                                            IconUri = "/",
                                            Updated = DateTime.Now
                                        }
                                };

            #endregion

            #region Populate Menus

            // J St. Menu
            var jMenu = onJ.Menus.SingleOrDefault(m => m.Name == "Menu");
            if (jMenu != null)
            {
                var bs = jMenu.Sections.SingleOrDefault(s => s.Name == "Breakfast Sandwiches");
                if (bs != null) bs.Items = hotBreakfastSandwiches;
                var hs = jMenu.Sections.SingleOrDefault(s => s.Name == "Hot Sandwiches");
                if (hs != null) hs.Items = hotSandwiches;
                var cs = jMenu.Sections.SingleOrDefault(s => s.Name == "Cold Sandwiches");
                if (cs != null) cs.Items = coldSandwiches;
                var sa = jMenu.Sections.SingleOrDefault(s => s.Name == "Salads");
                if (sa != null) sa.Items = salads;
                var yu = jMenu.Sections.SingleOrDefault(s => s.Name == "Yummies");
                if (yu != null) yu.Items = yummies;
                var bev = jMenu.Sections.SingleOrDefault(s => s.Name == "Beverages");
                if (bev != null) bev.Items = beverages;
            }
            // S St. Menu
            var sMenu = onS.Menus.SingleOrDefault(m => m.Name == "Menu");
            if (sMenu != null)
            {
                var bs = sMenu.Sections.SingleOrDefault(s => s.Name == "Breakfast Sandwiches");
                if (bs != null) bs.Items = hotBreakfastSandwiches;
                var hs = sMenu.Sections.SingleOrDefault(s => s.Name == "Hot Sandwiches");
                if (hs != null) hs.Items = hotSandwiches;
                var cs = sMenu.Sections.SingleOrDefault(s => s.Name == "Cold Sandwiches");
                if (cs != null) cs.Items = coldSandwiches;
                var sa = sMenu.Sections.SingleOrDefault(s => s.Name == "Salads");
                if (sa != null) sa.Items = salads;
                var yu = sMenu.Sections.SingleOrDefault(s => s.Name == "Yummies");
                if (yu != null) yu.Items = yummies;
                var bev = sMenu.Sections.SingleOrDefault(s => s.Name == "Beverages");
                if (bev != null) bev.Items = beverages;
            }

            

            #endregion

            session.Store(onS);
            session.Store(onJ);
            

            session.SaveChanges();
        }
    }
}