﻿
using System.Collections.Generic;
using DadsSandwiches.Models;

namespace DadsSandwiches.ViewModels
{
    public class AdminDashboard
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }
        public IEnumerable<CateringInquiry> CateringInquiries { get; set; }
    }
}