

// usage: log('inside coolFunc', this, arguments);
//window.log = function(){
//  log.history = log.history || [];   // store logs to an array for reference
//  log.history.push(arguments);
//  if(this.console) {
//      arguments.callee = arguments.callee.caller;
//      console.log( Array.prototype.slice.call(arguments) );
//  }
//};
//(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();)b[a]=b[a]||c})(window.console=window.console||{});


// Width & Height for Debugging Layout.
//$(function () {
//    var w = $(window).width();
//    var h = $(window).height();
//    var copyright = $('#copyright small').html();
//    $('#copyright small').html(copyright + ' -- WIDTH: ' + w + ', HEIGHT: ' + h);
//    $(window).resize(function () {
//        var w1 = $(window).width();
//        var h2 = $(window).height();
//        $('#copyright small').html(copyright + ' -- WIDTH: ' + w1 + ', HEIGHT: ' + h2);
//    });
//});
