﻿
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DadsSandwiches.Data;
using Raven.Abstractions.Data;
using Raven.Client.Document;
using Raven.Client.Indexes;

namespace DadsSandwiches
{
    public class DadsMvcApplication : HttpApplication
    {
        public static DocumentStore RavenStore { get; set; }
        
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Get single restaurant and menus
            routes.MapRoute("AdminDashboard", "admin", new { controller = "Admin", action = "Index" });
            routes.MapRoute("Catering", "catering", new { controller = "Home", action = "Catering" });
            routes.MapRoute("ContactUs", "contact-us", new { controller = "Home", action = "ContactUs" });
            routes.MapRoute("Restaurants", "restaurants", new { controller = "Home", action = "Restaurants" });
            routes.MapRoute("Restaurant", "{slug}", new { controller = "Home", action = "Restaurant" });
            
            // Default
            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }

        protected void Application_Start()
        {
            // RavenDb
            var parser = ConnectionStringParser<RavenConnectionStringOptions>.FromConnectionStringName("RavenDB");
            parser.Parse();
            RavenStore = new DocumentStore {
                ApiKey = parser.ConnectionStringOptions.ApiKey,
                Url = parser.ConnectionStringOptions.Url,
            };
            RavenStore.Initialize();
            IndexCreation.CreateIndexes(Assembly.GetCallingAssembly(), RavenStore);

            Seeder.DropData();
            Seeder.Seed();

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}