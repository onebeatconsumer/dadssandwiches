﻿using System;
using System.Collections.Generic;

namespace DadsSandwiches.Models
{
    public class Menu
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Updated { get; set; }        
        public IEnumerable<MenuSection> Sections { get; set; }
    }

    public class MenuSection
    {
        public string Name { get; set; }
        public int Order { get; set; }
        public IEnumerable<MenuItem> Items { get; set; }
    }

    public class MenuItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string IconUri { get; set; }
        public DateTime Updated { get; set; }
    }

}