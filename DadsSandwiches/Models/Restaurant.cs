﻿
using System.Collections.Generic;

namespace DadsSandwiches.Models
{
    public class Restaurant
    {
        public int Id { get; set; }
        
        public string Slug { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Hours { get; set; }
        public Address Address { get; set; }
        public string Phone { get; set; }

        public string Image { get; set; }
        public string MapUri { get; set; }

        public IEnumerable<Menu> Menus { get; set; }
    }

    public struct Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}