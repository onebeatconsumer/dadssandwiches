﻿using System;

namespace DadsSandwiches.Models
{
    public class CateringInquiry
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Contact Contact { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime InquiryDate { get; set; }
    }

    public struct Contact
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        
    }
}