﻿
using System.Linq;
using System.Web.Mvc;
using DadsSandwiches.Models;
using System.Net.Mail;

namespace DadsSandwiches.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Welcome to Dads Sandwiches!";
            return View();
        }

        public ActionResult Restaurants()
        {
            ViewBag.Title = "Dad's Sandwiches - Restaurants";
            var restaurants = DbSession.Query<Restaurant>().ToList().OrderBy(c => c.Id);
            return View(restaurants);
        }

        public ActionResult Restaurant(string slug)
        {
            var restaurant = DbSession.Query<Restaurant>().SingleOrDefault(r => r.Slug == slug);
            if (restaurant == null)
                return new HttpNotFoundResult();
            return View(restaurant);
        }

        public ActionResult Catering()
        {
            ViewBag.Title = "Dad's Sandwiches - Catering";
            return View();
        }
        [HttpPost]
        public ActionResult Catering(CateringInquiry inquiry)
        {
            DbSession.Store(inquiry);

            var client = new SmtpClient();
            var from = new MailAddress(inquiry.Contact.Email, inquiry.Contact.Name, System.Text.Encoding.UTF8);
            var to = new MailAddress("trey.white@fire.ca.gov");
            var message = new MailMessage(from, to)
                              {
                                  Subject = "Catering Inquiry: " + inquiry.Contact.Name + " on " + inquiry.EventDate,
                                  Body = "Catering Inquiry:\r\n\r\n" +
                                         "Name:" + inquiry.Contact.Name + "\r\n" +
                                         "Phone:" + inquiry.Contact.Phone + "\r\n" +
                                         "Email:" + inquiry.Contact.Email + "\r\n" +
                                         "Event Date:" + inquiry.EventDate + "\r\n" +
                                         "Event Description:" + inquiry.Description + "\r\n",
                                  SubjectEncoding = System.Text.Encoding.UTF8,
                                  BodyEncoding = System.Text.Encoding.UTF8
                              };
            
            //client.SendAsync(message, null);
            
            return View();
        }


        public ActionResult ContactUs()
        {
            ViewBag.Title = "Dad's Sandwiches - Contact Us";
            return View();
        }
    }
}
