﻿

using System.Web.Mvc;
using Raven.Client;

namespace DadsSandwiches.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IDocumentSession DbSession { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            DbSession = DadsMvcApplication.RavenStore.OpenSession();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.IsChildAction)
                return;
            using (DbSession) {
                if (filterContext.Exception != null)
                    return;
                if (DbSession != null)
                    DbSession.SaveChanges();
            }
        }

    }
}
