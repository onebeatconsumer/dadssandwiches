﻿
using System.Linq;
using System.Web.Mvc;
using DadsSandwiches.Models;
using DadsSandwiches.ViewModels;

namespace DadsSandwiches.Controllers
{
    public class AdminController : BaseController
    {
        public ActionResult Index()
        {
            var dashboard = new AdminDashboard
                                {
                                    Restaurants = DbSession.Query<Restaurant>().ToList(),
                                    CateringInquiries = DbSession.Query<CateringInquiry>().ToList()
                                };
            return View(dashboard);
        }

    }
}
